package main

import "core:os"
import "core:log"
import "core:mem"
import str "core:strings"
import vk "vendor:vulkan"
import glfw "vendor:glfw"
import c "core:c/libc"
import lib "core:dynlib"
import math "core:math/linalg"

MAX_FRAMES_IN_FLIGHT :: 2

Vertex :: struct {
  pos:   math.Vector2f32,
  color: math.Vector3f32,
}

vertex_get_binding_description :: proc() -> vk.VertexInputBindingDescription {
  binding_description := vk.VertexInputBindingDescription {
    binding   = 0,
    stride    = size_of(Vertex),
    inputRate = .VERTEX,
  }
  return binding_description
}


vertex_get_attribute_descriptions :: proc() -> [2]vk.VertexInputAttributeDescription {
  attribute_descriptions := [2]vk.VertexInputAttributeDescription{}
  attribute_descriptions[0].binding = 0
  attribute_descriptions[0].location = 0
  attribute_descriptions[0].format = .R32G32_SFLOAT
  attribute_descriptions[0].offset = cast(u32)offset_of(Vertex, pos)

  attribute_descriptions[1].binding = 0
  attribute_descriptions[1].location = 1
  attribute_descriptions[1].format = .R32G32B32_SFLOAT
  attribute_descriptions[1].offset = cast(u32)offset_of(Vertex, color)

  return attribute_descriptions
}

vertices := [4]Vertex{
  {{-0.5, -0.5}, {1.0, 0.0, 0.0}},
  {{0.5, -0.5}, {0.0, 1.0, 0.0}},
  {{0.5, 0.5}, {0.0, 0.0, 1.0}},
  {{-0.5, 0.5}, {1.0, 1.0, 1.0}},
}

indices := [6]u16{
  0, 1, 2, 2, 3, 0,
}

QueuesIndices :: struct {
  graphics_family: Maybe(u32),
  present_family:  Maybe(u32),
}

SwapChainSupportDetails :: struct {
  capabilities:  vk.SurfaceCapabilitiesKHR,
  formats:       [dynamic]vk.SurfaceFormatKHR,
  present_modes: [dynamic]vk.PresentModeKHR,
}

Application :: struct {
  enable_validation_layers:   bool,
  framebuffer_resized:        bool,
  width:                      i32,
  height:                     i32,
  window:                     glfw.WindowHandle,
  validation_layers:          []cstring,
  device_extensions:          []cstring,
  instance:                   vk.Instance,
  physical_device:            vk.PhysicalDevice,
  device:                     vk.Device,
  surface:                    vk.SurfaceKHR,
  surface_format:             vk.SurfaceFormatKHR,
  swap_chain:                 vk.SwapchainKHR,
  graphics_queue:             vk.Queue,
  present_queue:              vk.Queue,
  swap_chain_extent:          vk.Extent2D,
  swap_chain_format:          vk.Format,
  swap_chain_images:          [dynamic]vk.Image,
  swap_chain_image_views:     [dynamic]vk.ImageView,
  swap_chain_framebuffers:    [dynamic]vk.Framebuffer,
  render_pass:                vk.RenderPass,
  pipeline_layout:            vk.PipelineLayout,
  graphics_pipeline:          vk.Pipeline,
  vertex_buffer:              vk.Buffer,
  vertex_buffer_memory:       vk.DeviceMemory,
  index_buffer:               vk.Buffer,
  index_buffer_memory:        vk.DeviceMemory,
  command_pool:               vk.CommandPool,
  command_buffers:            [dynamic]vk.CommandBuffer,
  image_available_semaphores: [dynamic]vk.Semaphore,
  render_finished_semaphores: [dynamic]vk.Semaphore,
  in_flight_fences:           [dynamic]vk.Fence,
  debug_messenger:            vk.DebugUtilsMessengerEXT,
}

application: Application

current_frame := 0

main :: proc() {
  context = init_logger("vulkan_triangle.log", "Vulkan Triangle")
  defer deinit_logger()

  init_application()

  init_window()

  init_vulkan()

  main_loop()

  cleanup()
}

init_application :: proc() {
  application.width = 1280
  application.height = 720
  when ODIN_DEBUG {
    validation_layers := [dynamic]cstring{
      str.clone_to_cstring("VK_LAYER_KHRONOS_validation"),
    }

    application.enable_validation_layers = true
    application.validation_layers = validation_layers[:]
  } else {
    application.enable_validation_layers = false
    application.validation_layers = nil
  }
  device_extensions := [dynamic]cstring{vk.KHR_SWAPCHAIN_EXTENSION_NAME}
  application.device_extensions = device_extensions[:]
  application.framebuffer_resized = false
}

init_window :: proc() {
  if glfw.Init() == 0 {
    log.error("glfw: initalizing error")
    os.exit(-1)
  }

  glfw.WindowHint(glfw.CLIENT_API, glfw.NO_API)
  glfw.WindowHint(glfw.RESIZABLE, 1)

  application.window = glfw.CreateWindow(
    application.width,
    application.height,
    "Vulkan demo",
    nil,
    nil,
  )
  glfw.SetWindowUserPointer(application.window, &application)
  glfw.SetFramebufferSizeCallback(application.window, framebuffer_resize_callback)
}

init_vulkan :: proc() {
  vk.load_proc_addresses(glfw_set_proc_address)
  create_instance()
  //setup_debug_messenger()
  create_window_surface()
  
  pick_physical_device()
  create_logical_device()
  create_command_pool()

  create_swap_chain()
  create_image_views()
  create_render_pass()
  
  create_framebuffers()
  create_command_buffers()
  create_sync_objects()

  create_graphic_pipeline()

  create_vertex_buffer()
  create_index_buffer()
}

main_loop :: proc() {
  for !glfw.WindowShouldClose(application.window) {
    glfw.PollEvents()
    draw_frame()
  }
}

draw_frame :: proc() {
  uint64_max := max(u64)
  vk.WaitForFences(
    application.device,
    1,
    &application.in_flight_fences[current_frame],
    true,
    uint64_max,
  )

  image_index: u32

  result := vk.AcquireNextImageKHR(
    application.device,
    application.swap_chain,
    uint64_max,
    application.image_available_semaphores[current_frame],
    0,
    &image_index,
  )
  if result == .ERROR_OUT_OF_DATE_KHR || result == .SUBOPTIMAL_KHR {
    recreate_swap_chain()
    application.framebuffer_resized = false
    return
  } else if result != .SUCCESS && result != .SUBOPTIMAL_KHR {
    log.error("failed to acquire swap chain image!")
    os.exit(-1)
  }

  // Only reset the fence if we are submitting work
  // vulkan_fence_reset
  vk.ResetFences(application.device, 1, &application.in_flight_fences[current_frame])

  vk.ResetCommandBuffer(application.command_buffers[current_frame], {})
  record_command_buffer(application.command_buffers[current_frame], image_index)

  signal_semaphores := []vk.Semaphore{
    application.render_finished_semaphores[current_frame],
  }
  wait_semaphores := []vk.Semaphore{application.image_available_semaphores[current_frame]}
  wait_stages := []vk.PipelineStageFlags{{.COLOR_ATTACHMENT_OUTPUT}}
  submit_info := vk.SubmitInfo {
    sType                = .SUBMIT_INFO,
    
    commandBufferCount   = 1,
    pCommandBuffers      = &application.command_buffers[current_frame],

    signalSemaphoreCount = cast(u32)len(signal_semaphores),
    pSignalSemaphores    = raw_data(signal_semaphores),

    waitSemaphoreCount   = cast(u32)len(wait_semaphores),
    pWaitSemaphores      = raw_data(wait_semaphores),
    pWaitDstStageMask    = raw_data(wait_stages),
  }

  if vk.QueueSubmit(
       application.graphics_queue,
       1,
       &submit_info,
       application.in_flight_fences[current_frame],
     ) != .SUCCESS {
    log.error("failed to submit draw command buffer!")
    os.exit(-1)
  }

  // vulkan_swapchain_present
  {
    swap_chains := []vk.SwapchainKHR{application.swap_chain}

    present_info := vk.PresentInfoKHR {
      sType              = .PRESENT_INFO_KHR,
      waitSemaphoreCount = cast(u32)len(signal_semaphores),
      pWaitSemaphores    = raw_data(signal_semaphores),
      swapchainCount     = cast(u32)len(swap_chains),
      pSwapchains        = raw_data(swap_chains),
      pImageIndices      = &image_index,
    }

    vk.QueuePresentKHR(application.present_queue, &present_info)

    current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT
  }
}

cleanup :: proc() {
  vk.DeviceWaitIdle(application.device)

  cleanup_swap_chain()

  vk.DestroyBuffer(application.device, application.vertex_buffer, nil)
  vk.FreeMemory(application.device, application.vertex_buffer_memory, nil)

  vk.DestroyBuffer(application.device, application.index_buffer, nil)
  vk.FreeMemory(application.device, application.index_buffer_memory, nil)


  for i in 0 ..< MAX_FRAMES_IN_FLIGHT {
    vk.DestroySemaphore(application.device, application.image_available_semaphores[i], nil)
    vk.DestroySemaphore(application.device, application.render_finished_semaphores[i], nil)
    vk.DestroyFence(application.device, application.in_flight_fences[i], nil)
  }

  vk.DestroyCommandPool(application.device, application.command_pool, nil)

  vk.DestroyDevice(application.device, nil)

  /*
    if application.enable_validation_layers {
        vk.DestroyDebugUtilsMessengerEXT(application.instance, application.debug_messenger, nil)
    }
    */

  vk.DestroySurfaceKHR(application.instance, application.surface, nil)
  vk.DestroyInstance(application.instance, nil)
  glfw.DestroyWindow(application.window)
  glfw.Terminate()
}

create_instance :: proc() {
  if application.enable_validation_layers && !check_validation_layer_support() {
    log.error("validation layers requested, but not available!")
    os.exit(-1)
  }

  extensions := get_required_extensions()

  applicationInfo := vk.ApplicationInfo {
    // sType is mandatory
    sType              = .APPLICATION_INFO,
    // pNext is mandatory
    pNext              = nil,
    // The name of our application
    pApplicationName   = "Vulkan demo",
    applicationVersion = vk.MAKE_VERSION(1, 0, 0),
    pEngineName        = "No engine",
    engineVersion      = vk.MAKE_VERSION(1, 0, 0),
    apiVersion         = vk.API_VERSION_1_0,
  }

  instance_create_info := vk.InstanceCreateInfo {
    .INSTANCE_CREATE_INFO, // sType:                   StructureType 
    nil, // pNext:                   rawptr
    {}, // flags:                   InstanceCreateFlags
    &applicationInfo, // pApplicationInfo:        ^ApplicationInfo
    0, // enabledLayerCount:       u32
    nil, // ppEnabledLayerNames:     [^]cstring
    cast(u32)len(extensions), // enabledExtensionCount:   u32
    raw_data(extensions), // ppEnabledExtensionNames: [^]cstring
  }

  if application.enable_validation_layers {
    instance_create_info.enabledLayerCount = cast(u32)len(application.validation_layers)
    instance_create_info.ppEnabledLayerNames = raw_data(application.validation_layers)
  }

  inst_err := vk.CreateInstance(&instance_create_info, nil, &application.instance)
  if inst_err != .SUCCESS {
    log.errorf("vulkan: instance failed with result %v", inst_err)
    os.exit(-1)
  }
}

setup_debug_messenger :: proc() {
  if !application.enable_validation_layers do return
  
  log_severity := vk.DebugUtilsMessageSeverityFlagsEXT{.ERROR, .WARNING, .INFO, .VERBOSE}

  debug_create_info := vk.DebugUtilsMessengerCreateInfoEXT {
    sType = .DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
    messageSeverity = log_severity,
    messageType = {.GENERAL, .PERFORMANCE, .VALIDATION},
    pfnUserCallback = debug_callback,
    pUserData = nil,
  }

  if vk.CreateDebugUtilsMessengerEXT(
    application.instance,
    &debug_create_info,
    nil,
    &application.debug_messenger,
  ) != .SUCCESS {
    log.error("failed to set up debug messenger!")
    os.exit(-1)
  }
}

pick_physical_device :: proc() {
  device_count: u32 = 0
  vk.EnumeratePhysicalDevices(application.instance, &device_count, nil)
  if (device_count == 0) {
    log.error("failed to find GPUs with Vulkan support!")
    os.exit(-1)
  }

  devices := make([dynamic]vk.PhysicalDevice, device_count)
  vk.EnumeratePhysicalDevices(application.instance, &device_count, raw_data(devices))

  for device in devices {
    if is_device_suitable(device) {
      application.physical_device = device
      break
    }
  }

  if application.physical_device == nil {
    log.error("failed to find a suitable GPU!")
    os.exit(-1)
  }
}

create_logical_device :: proc() {
  queues_indices := find_queue_families(application.physical_device)

  unique_queue_families := []u32{
    queues_indices.graphics_family.?,
    queues_indices.present_family.?,
  }
  queue_create_infos := [dynamic]vk.DeviceQueueCreateInfo{}

  queue_priority: f32 = 1.0
  for queue_family in unique_queue_families {
    queue_create_info := vk.DeviceQueueCreateInfo {
      sType            = .DEVICE_QUEUE_CREATE_INFO,
      queueFamilyIndex = queue_family,
      queueCount       = 1,
      pQueuePriorities = &queue_priority,
    }

    append(&queue_create_infos, queue_create_info)
  }

  device_features := vk.PhysicalDeviceFeatures{}

  create_info := vk.DeviceCreateInfo {
    sType                   = .DEVICE_CREATE_INFO,
    queueCreateInfoCount    = cast(u32)len(queue_create_infos),
    pQueueCreateInfos       = raw_data(queue_create_infos),
    pEnabledFeatures        = &device_features,
    enabledExtensionCount   = cast(u32)len(application.device_extensions),
    ppEnabledExtensionNames = raw_data(application.device_extensions),
  }

  if application.enable_validation_layers {
    create_info.enabledLayerCount = cast(u32)len(application.validation_layers)
    create_info.ppEnabledLayerNames = raw_data(application.validation_layers)
  } else {
    create_info.enabledLayerCount = 0
  }

  if vk.CreateDevice(
       application.physical_device,
       &create_info,
       nil,
       &application.device,
     ) != .SUCCESS {
    log.error("failed to create logical device!")
    os.exit(-1)
  }

  vk.GetDeviceQueue(
    application.device,
    queues_indices.graphics_family.?,
    0,
    &application.graphics_queue,
  )
  vk.GetDeviceQueue(
    application.device,
    queues_indices.present_family.?,
    0,
    &application.present_queue,
  )
}

create_window_surface :: proc() {
  if glfw.CreateWindowSurface(
       application.instance,
       application.window,
       nil,
       &application.surface,
     ) != .SUCCESS {
    log.error("failed to create window surface!")
    os.exit(-1)
  }
}

create_swap_chain :: proc() {
  swap_chain_support := query_swap_chain_support(application.physical_device)

  application.surface_format = choose_swap_surface_format(swap_chain_support.formats[:])
  present_mode := choose_swap_present_mode(swap_chain_support.present_modes[:])
  application.swap_chain_extent = choose_swap_extent(swap_chain_support.capabilities)
  application.swap_chain_format = application.surface_format.format

  image_count := swap_chain_support.capabilities.minImageCount + 1
  if swap_chain_support.capabilities.maxImageCount > 0 && image_count > swap_chain_support.capabilities.maxImageCount {
    image_count = swap_chain_support.capabilities.maxImageCount
  }

  create_info := vk.SwapchainCreateInfoKHR {
    sType = .SWAPCHAIN_CREATE_INFO_KHR,
    surface = application.surface,
    minImageCount = image_count,
    imageFormat = application.surface_format.format,
    imageColorSpace = application.surface_format.colorSpace,
    imageExtent = application.swap_chain_extent,
    imageArrayLayers = 1,
    imageUsage = {.COLOR_ATTACHMENT},
  }

  indices := find_queue_families(application.physical_device)
  queue_family_indices := []u32{indices.graphics_family.?, indices.present_family.?}

  if (indices.graphics_family != indices.present_family) {
    create_info.imageSharingMode = .CONCURRENT
    create_info.queueFamilyIndexCount = 2
    create_info.pQueueFamilyIndices = raw_data(queue_family_indices)
  } else {
    create_info.imageSharingMode = .EXCLUSIVE
    create_info.queueFamilyIndexCount = 0 // Optional
    create_info.pQueueFamilyIndices = nil // Optional
  }
  create_info.preTransform = swap_chain_support.capabilities.currentTransform
  create_info.compositeAlpha = {.OPAQUE}
  create_info.presentMode = present_mode
  create_info.clipped = true
  create_info.oldSwapchain = 0

  if result := vk.CreateSwapchainKHR(
       application.device,
       &create_info,
       nil,
       &application.swap_chain,
     ); result != .SUCCESS {
    log.errorf("failed to create swap chain! code %d", result)
    os.exit(-1)
  }

  vk.GetSwapchainImagesKHR(application.device, application.swap_chain, &image_count, nil)

  resize(&application.swap_chain_images, cast(int)image_count)

  vk.GetSwapchainImagesKHR(
    application.device,
    application.swap_chain,
    &image_count,
    raw_data(application.swap_chain_images),
  )
}

create_image_views :: proc() {
  resize(&application.swap_chain_image_views, len(application.swap_chain_images))
  for sc_image, index in application.swap_chain_images {
    create_info := vk.ImageViewCreateInfo {
      sType = .IMAGE_VIEW_CREATE_INFO,
      image = sc_image,
      viewType = .D2,
      format = application.swap_chain_format,
      components = {r = .IDENTITY, g = .IDENTITY, b = .IDENTITY, a = .IDENTITY},
      subresourceRange = {
        aspectMask = {.COLOR},
        baseMipLevel = 0,
        levelCount = 1,
        baseArrayLayer = 0,
        layerCount = 1,
      },
    }

    if vk.CreateImageView(
         application.device,
         &create_info,
         nil,
         &application.swap_chain_image_views[index],
       ) != .SUCCESS {
      log.error("failed to create image views!")
      os.exit(-1)
    }
  }
}

create_graphic_pipeline :: proc() {
  vertex_shader_code, vert_ok := read_file("shaders/shader.vert.spv")
  if !vert_ok {
    log.error("Failed to open vertex shader")
    os.exit(-1)
  }

  fragement_shader_code, frag_ok := read_file("shaders/shader.frag.spv")
  if !frag_ok {
    log.error("Failed to open vertex shader")
    os.exit(-1)
  }

  vert_shader_module := create_shader_module(vertex_shader_code)
  defer vk.DestroyShaderModule(application.device, vert_shader_module, nil)

  frag_shader_module := create_shader_module(fragement_shader_code)
  defer vk.DestroyShaderModule(application.device, frag_shader_module, nil)

  vert_shader_stage_info := vk.PipelineShaderStageCreateInfo {
    sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
    stage = {.VERTEX},
    module = vert_shader_module,
    pName = "main",
  }

  frag_shader_stage_info := vk.PipelineShaderStageCreateInfo {
    sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
    stage = {.FRAGMENT},
    module = frag_shader_module,
    pName = "main",
  }

  shader_stages := []vk.PipelineShaderStageCreateInfo{
    vert_shader_stage_info,
    frag_shader_stage_info,
  }

  attribute_descriptions := vertex_get_attribute_descriptions()
  binding_description := vertex_get_binding_description()
  vertex_input_info := vk.PipelineVertexInputStateCreateInfo {
    sType                           = .PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    vertexBindingDescriptionCount   = 1,
    pVertexBindingDescriptions      = &binding_description,
    vertexAttributeDescriptionCount = cast(u32)len(attribute_descriptions),
    pVertexAttributeDescriptions    = raw_data(attribute_descriptions[:]),
  }

  input_assembly := vk.PipelineInputAssemblyStateCreateInfo {
    sType                  = .PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
    topology               = .TRIANGLE_LIST,
    primitiveRestartEnable = false,
  }

  viewport := vk.Viewport {
    x        = 0.0,
    y        = 0.0,
    width    = cast(f32)application.swap_chain_extent.width,
    height   = cast(f32)application.swap_chain_extent.height,
    minDepth = 0.0,
    maxDepth = 1.0,
  }

  scissor := vk.Rect2D {
    offset = {0, 0},
    extent = application.swap_chain_extent,
  }

  viewport_state := vk.PipelineViewportStateCreateInfo {
    sType         = .PIPELINE_VIEWPORT_STATE_CREATE_INFO,
    viewportCount = 1,
    pViewports    = &viewport,
    scissorCount  = 1,
    pScissors     = &scissor,
  }

  rasterizer := vk.PipelineRasterizationStateCreateInfo {
    sType = .PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
    depthClampEnable = false,
    rasterizerDiscardEnable = false,
    polygonMode = .FILL,
    lineWidth = 1.0,
    cullMode = {.BACK},
    frontFace = .CLOCKWISE,
    depthBiasEnable = false,
    depthBiasConstantFactor = 0.0,
    depthBiasClamp = 0.0,
    depthBiasSlopeFactor = 0.0,
  }

  multisampling := vk.PipelineMultisampleStateCreateInfo {
    sType = .PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
    sampleShadingEnable = false,
    rasterizationSamples = {._1},
    minSampleShading = 1.0,
    pSampleMask = nil,
    alphaToCoverageEnable = false,
    alphaToOneEnable = false,
  }

  color_blend_attachment := vk.PipelineColorBlendAttachmentState {
    colorWriteMask = {.R, .G, .B, .A},
    blendEnable = false,
    srcColorBlendFactor = .ONE,
    dstColorBlendFactor = .ZERO,
    colorBlendOp = .ADD,
    srcAlphaBlendFactor = .ONE,
    dstAlphaBlendFactor = .ZERO,
    alphaBlendOp = .ADD,
  }

  color_blending := vk.PipelineColorBlendStateCreateInfo {
    sType = .PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
    logicOpEnable = false,
    logicOp = .COPY,
    attachmentCount = 1,
    pAttachments = &color_blend_attachment,
    blendConstants = {0.0, 0.0, 0.0, 0.0},
  }

  pipeline_layout_info := vk.PipelineLayoutCreateInfo {
    sType                  = .PIPELINE_LAYOUT_CREATE_INFO,
    setLayoutCount         = 0,
    pSetLayouts            = nil,
    pushConstantRangeCount = 0,
    pPushConstantRanges    = nil,
  }

  if vk.CreatePipelineLayout(
       application.device,
       &pipeline_layout_info,
       nil,
       &application.pipeline_layout,
     ) != .SUCCESS {
    log.error("failed to create pipeline layout!")
    os.exit(-1)
  }

  pipeline_info := vk.GraphicsPipelineCreateInfo {
    sType               = .GRAPHICS_PIPELINE_CREATE_INFO,
    stageCount          = 2,
    pStages             = raw_data(shader_stages),
    pVertexInputState   = &vertex_input_info,
    pInputAssemblyState = &input_assembly,
    pViewportState      = &viewport_state,
    pRasterizationState = &rasterizer,
    pMultisampleState   = &multisampling,
    pDepthStencilState  = nil,
    pColorBlendState    = &color_blending,
    pDynamicState       = nil,
    layout              = application.pipeline_layout,
    renderPass          = application.render_pass,
    subpass             = 0,
    basePipelineHandle  = 0,
    basePipelineIndex   = -1,
  }

  if vk.CreateGraphicsPipelines(
       application.device,
       0,
       1,
       &pipeline_info,
       nil,
       &application.graphics_pipeline,
     ) != .SUCCESS {
    log.error("failed to create graphics pipeline!")
    os.exit(-1)
  }
}

create_render_pass :: proc() {
  color_attachment := vk.AttachmentDescription {
    format = application.swap_chain_format,
    samples = {._1},
    loadOp = .CLEAR,
    storeOp = .STORE,
    stencilLoadOp = .DONT_CARE,
    stencilStoreOp = .DONT_CARE,
    initialLayout = .UNDEFINED,
    finalLayout = .PRESENT_SRC_KHR,
  }

  color_attachment_ref := vk.AttachmentReference {
    attachment = 0,
    layout     = .COLOR_ATTACHMENT_OPTIMAL,
  }

  subpass := vk.SubpassDescription {
    pipelineBindPoint    = .GRAPHICS,
    colorAttachmentCount = 1,
    pColorAttachments    = &color_attachment_ref,
  }


  dependency := vk.SubpassDependency {
    srcSubpass = vk.SUBPASS_EXTERNAL,
    dstSubpass = 0,
    srcStageMask = {.COLOR_ATTACHMENT_OUTPUT},
    srcAccessMask = {},
    dstStageMask = {.COLOR_ATTACHMENT_OUTPUT},
    dstAccessMask = {.COLOR_ATTACHMENT_WRITE},
  }

  render_pass_info := vk.RenderPassCreateInfo {
    sType           = .RENDER_PASS_CREATE_INFO,
    attachmentCount = 1,
    pAttachments    = &color_attachment,
    subpassCount    = 1,
    pSubpasses      = &subpass,
    dependencyCount = 1,
    pDependencies   = &dependency,
  }

  if vk.CreateRenderPass(
       application.device,
       &render_pass_info,
       nil,
       &application.render_pass,
     ) != .SUCCESS {
    log.error("failed to create render pass!")
    os.exit(-1)
  }
}

create_framebuffers :: proc() {
  resize(&application.swap_chain_framebuffers, len(application.swap_chain_image_views))

  for _, i in application.swap_chain_image_views {
    attachments := []vk.ImageView{application.swap_chain_image_views[i]}

    framebuffer_info := vk.FramebufferCreateInfo {
      sType           = .FRAMEBUFFER_CREATE_INFO,
      renderPass      = application.render_pass,
      attachmentCount = 1,
      pAttachments    = raw_data(attachments),
      width           = application.swap_chain_extent.width,
      height          = application.swap_chain_extent.height,
      layers          = 1,
    }

    if vk.CreateFramebuffer(
         application.device,
         &framebuffer_info,
         nil,
         &application.swap_chain_framebuffers[i],
       ) != .SUCCESS {
      log.error("failed to create framebuffer!")
      os.exit(-1)
    }
  }
}

create_command_pool :: proc() {
  queue_family_indices := find_queue_families(application.physical_device)

  pool_info := vk.CommandPoolCreateInfo {
    sType = .COMMAND_POOL_CREATE_INFO,
    flags = {.RESET_COMMAND_BUFFER},
    queueFamilyIndex = queue_family_indices.graphics_family.?,
  }

  if vk.CreateCommandPool(
       application.device,
       &pool_info,
       nil,
       &application.command_pool,
     ) != .SUCCESS {
    log.error("failed to create command pool!")
    os.exit(-1)
  }
}

create_vertex_buffer :: proc() {
    buffer_size := cast(vk.DeviceSize) size_of(Vertex) * len(vertices)

    staging_buffer := vk.Buffer{}
    staging_buffer_memory := vk.DeviceMemory{}
    create_buffer(buffer_size, {.TRANSFER_SRC}, {.HOST_VISIBLE, .HOST_COHERENT}, &staging_buffer, &staging_buffer_memory)

    data: rawptr
    vk.MapMemory(application.device, staging_buffer_memory, 0, buffer_size, {}, &data)
    
    mem.copy(data, cast(rawptr)raw_data(vertices[:]), cast(int)buffer_size)

    create_buffer(buffer_size, {.TRANSFER_DST, .VERTEX_BUFFER}, {.DEVICE_LOCAL}, &application.vertex_buffer, &application.vertex_buffer_memory)

    copy_buffer(staging_buffer, application.vertex_buffer, buffer_size);

    vk.UnmapMemory(application.device, staging_buffer_memory)

    vk.DestroyBuffer(application.device, staging_buffer, nil)
    vk.FreeMemory(application.device, staging_buffer_memory, nil)
}

create_index_buffer :: proc () {
  buffer_size := cast(vk.DeviceSize) size_of(u16) * len(indices)

  staging_buffer: vk.Buffer
  staging_buffer_memory: vk.DeviceMemory
  create_buffer(buffer_size, {.TRANSFER_SRC}, {.HOST_VISIBLE, .HOST_COHERENT}, &staging_buffer, &staging_buffer_memory)

  data: rawptr
  vk.MapMemory(application.device, staging_buffer_memory, 0, buffer_size, {}, &data)
  mem.copy(data, raw_data(indices[:]), cast(int)buffer_size)
  vk.UnmapMemory(application.device, staging_buffer_memory)

  create_buffer(buffer_size, {.TRANSFER_DST, .INDEX_BUFFER}, {.DEVICE_LOCAL}, &application.index_buffer, &application.index_buffer_memory)

  copy_buffer(staging_buffer, application.index_buffer, buffer_size)

  vk.DestroyBuffer(application.device,staging_buffer, nil)
  vk.FreeMemory(application.device, staging_buffer_memory, nil)
}

copy_buffer :: proc (src_buffer: vk.Buffer, dst_buffer: vk.Buffer, size: vk.DeviceSize) {
  alloc_info := vk.CommandBufferAllocateInfo {
    sType = .COMMAND_BUFFER_ALLOCATE_INFO,
    level = .PRIMARY,
    commandPool = application.command_pool,
    commandBufferCount = 1,
  }
  
  command_buffer: vk.CommandBuffer
  vk.AllocateCommandBuffers(application.device, &alloc_info, &command_buffer)

  begin_info := vk.CommandBufferBeginInfo{
    sType = .COMMAND_BUFFER_BEGIN_INFO,
    flags = {.ONE_TIME_SUBMIT},
  }

  vk.BeginCommandBuffer(command_buffer, &begin_info)

  copy_region := vk.BufferCopy{
    size = size,
  }

  vk.CmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region)

  vk.EndCommandBuffer(command_buffer)

  submit_info := vk.SubmitInfo{
    sType = .SUBMIT_INFO,
    commandBufferCount = 1,
    pCommandBuffers = &command_buffer,
  }

  vk.QueueSubmit(application.graphics_queue, 1, &submit_info, {})
  vk.QueueWaitIdle(application.graphics_queue)

  vk.FreeCommandBuffers(application.device, application.command_pool, 1, &command_buffer)
}

create_buffer :: proc (size: vk.DeviceSize, usage: vk.BufferUsageFlags, properties: vk.MemoryPropertyFlags, buffer: ^vk.Buffer, buffer_memory: ^vk.DeviceMemory) {
  buffer_info := vk.BufferCreateInfo {
    sType = .BUFFER_CREATE_INFO,
    sharingMode = .EXCLUSIVE,
    usage = usage,
    size = size,
  }

  if vk.CreateBuffer(application.device, &buffer_info, nil, buffer) != .SUCCESS {
    log.panic("failed to create vertex buffer!")
  }

  mem_requirements: vk.MemoryRequirements
  vk.GetBufferMemoryRequirements(application.device, buffer^, &mem_requirements)
    
  alloc_info := vk.MemoryAllocateInfo{
    sType = .MEMORY_ALLOCATE_INFO,
    allocationSize = mem_requirements.size,
    memoryTypeIndex = find_memory_index(mem_requirements.memoryTypeBits, properties),
  }

  if vk.AllocateMemory(application.device, &alloc_info, nil, buffer_memory) != .SUCCESS {
    log.panic("failed to allocate vertex buffer memory!")
  }
 
  vk.BindBufferMemory(application.device, buffer^, buffer_memory^, 0)
}

find_memory_index :: proc(type_filter: u32, property_flags: vk.MemoryPropertyFlags) -> u32 {
  memory_properties: vk.PhysicalDeviceMemoryProperties
  vk.GetPhysicalDeviceMemoryProperties(application.physical_device, &memory_properties)

  for i in 0 ..< memory_properties.memoryTypeCount {
    // Check each memory type to see if its bit is set to 1.
    type_filter_val: u32 = (1 << i)
    if (type_filter & type_filter_val) == type_filter_val && (memory_properties.memoryTypes[i].propertyFlags &
       property_flags) == property_flags {
      return i
    }
  }

  log.panic("Unable to find suitable memory type!")
}

create_command_buffers :: proc() {
  resize(&application.command_buffers, MAX_FRAMES_IN_FLIGHT)

  alloc_info := vk.CommandBufferAllocateInfo {
    sType              = .COMMAND_BUFFER_ALLOCATE_INFO,
    commandPool        = application.command_pool,
    level              = .PRIMARY,
    commandBufferCount = cast(u32)len(application.command_buffers),
  }
  if vk.AllocateCommandBuffers(
       application.device,
       &alloc_info,
       raw_data(application.command_buffers),
     ) != .SUCCESS {
    log.error("failed to allocate command buffers!")
    os.exit(-1)
  }
}

create_sync_objects :: proc() {
  resize(&application.image_available_semaphores, MAX_FRAMES_IN_FLIGHT)
  resize(&application.render_finished_semaphores, MAX_FRAMES_IN_FLIGHT)
  resize(&application.in_flight_fences, MAX_FRAMES_IN_FLIGHT)

  semaphore_info := vk.SemaphoreCreateInfo {
    sType = .SEMAPHORE_CREATE_INFO,
  }

  fence_info := vk.FenceCreateInfo {
    sType = .FENCE_CREATE_INFO,
    flags = {.SIGNALED},
  }

  for i in 0 ..< MAX_FRAMES_IN_FLIGHT {
    if vk.CreateSemaphore(
         application.device,
         &semaphore_info,
         nil,
         &application.image_available_semaphores[i],
       ) != .SUCCESS || vk.CreateSemaphore(
         application.device,
         &semaphore_info,
         nil,
         &application.render_finished_semaphores[i],
       ) != .SUCCESS || vk.CreateFence(
         application.device,
         &fence_info,
         nil,
         &application.in_flight_fences[i],
       ) != .SUCCESS {
      log.error("failed to create semaphores!")
      os.exit(-1)
    }
  }
}

cleanup_swap_chain :: proc() {
  for swap_chain_framebuffer in application.swap_chain_framebuffers {
    vk.DestroyFramebuffer(application.device, swap_chain_framebuffer, nil)
  }

  vk.DestroyPipeline(application.device, application.graphics_pipeline, nil)
  vk.DestroyPipelineLayout(application.device, application.pipeline_layout, nil)
  vk.DestroyRenderPass(application.device, application.render_pass, nil)

  for swap_chain_image_view in application.swap_chain_image_views {
    vk.DestroyImageView(application.device, swap_chain_image_view, nil)
  }

  vk.DestroySwapchainKHR(application.device, application.swap_chain, nil)
}

recreate_swap_chain :: proc() {
  width, height := glfw.GetFramebufferSize(application.window)
  for width == 0 || height == 0 {
    width, height = glfw.GetFramebufferSize(application.window)
    glfw.WaitEvents()
  }

  vk.DeviceWaitIdle(application.device)

  cleanup_swap_chain()

  create_swap_chain()
  create_image_views()
  create_render_pass()
  create_graphic_pipeline()
  create_framebuffers()
}

framebuffer_resize_callback :: proc "c" (
  window: glfw.WindowHandle,
  width,
  height: c.int,
) {
  app := cast(^Application)glfw.GetWindowUserPointer(window)
  app.framebuffer_resized = true
}

record_command_buffer :: proc(command_buffer: vk.CommandBuffer, image_index: u32) {
  begin_info := vk.CommandBufferBeginInfo {
    sType = .COMMAND_BUFFER_BEGIN_INFO,
    flags = {.ONE_TIME_SUBMIT},
    pInheritanceInfo = nil,
  }

  if vk.BeginCommandBuffer(command_buffer, &begin_info) != .SUCCESS {
    log.error("failed to begin recording command buffer!")
    os.exit(-1)
  }

  clear_color := vk.ClearValue {
    color = {float32 = {0.0, 0.0, 0.0, 1.0}},
  }

  render_pass_info := vk.RenderPassBeginInfo {
    sType = .RENDER_PASS_BEGIN_INFO,
    renderPass = application.render_pass,
    framebuffer = application.swap_chain_framebuffers[image_index],
    renderArea = {{0, 0}, application.swap_chain_extent},
    clearValueCount = 1,
    pClearValues = &clear_color,
  }

  vk.CmdBeginRenderPass(command_buffer, &render_pass_info, .INLINE)
  
  vertex_buffers := []vk.Buffer{application.vertex_buffer}
  offsets := []vk.DeviceSize{0}

  vk.CmdBindPipeline(command_buffer, .GRAPHICS, application.graphics_pipeline);
  vk.CmdBindVertexBuffers(command_buffer, 0, 1, raw_data(vertex_buffers), raw_data(offsets))

  vk.CmdBindIndexBuffer(command_buffer, application.index_buffer, 0, .UINT16)

  vk.CmdDraw(command_buffer, cast(u32)len(vertices), 1, 0, 0)
  vk.CmdDrawIndexed(command_buffer, cast(u32) len(indices), 1, 0, 0, 0)

  vk.CmdEndRenderPass(command_buffer)

  if vk.EndCommandBuffer(command_buffer) != .SUCCESS {
    log.error("failed to record command buffer!")
    os.exit(-1)
  }
}

create_shader_module :: proc(code: []byte) -> vk.ShaderModule {
  create_info := vk.ShaderModuleCreateInfo {
    sType    = .SHADER_MODULE_CREATE_INFO,
    codeSize = len(code),
    pCode    = cast(^u32)raw_data(code),
  }

  shader_module: vk.ShaderModule
  if (vk.CreateShaderModule(application.device, &create_info, nil, &shader_module) != .SUCCESS) {
    log.error("failed to create shader module!")
    os.exit(-1)
  }

  return shader_module
}

is_device_suitable :: proc(device: vk.PhysicalDevice) -> bool {
  device_properties: vk.PhysicalDeviceProperties
  vk.GetPhysicalDeviceProperties(device, &device_properties)

  device_features: vk.PhysicalDeviceFeatures
  vk.GetPhysicalDeviceFeatures(device, &device_features)

  is_suitable := device_properties.deviceType == .DISCRETE_GPU && cast(bool)device_features.geometryShader
  queues_indices := find_queue_families(device)

  device_extentions_supported := check_device_extension_support(device)

  swapChainAdequate := false
  if (device_extentions_supported) {
    swap_chain_support := query_swap_chain_support(device)
    swapChainAdequate = len(swap_chain_support.formats) > 0 && len(
                       swap_chain_support.present_modes,
                     ) > 0
  }

  return is_suitable && device_extentions_supported && swapChainAdequate && are_queues_complete(
    queues_indices,
  )
}

choose_swap_surface_format :: proc(
  available_formats: []vk.SurfaceFormatKHR,
) -> vk.SurfaceFormatKHR {
  for available_format in available_formats {
    if available_format.format == .B8G8R8A8_SRGB && available_format.colorSpace == .SRGB_NONLINEAR {
      return available_format
    }
  }

  return available_formats[0]
}

choose_swap_present_mode :: proc(
  available_present_modes: []vk.PresentModeKHR,
) -> vk.PresentModeKHR {
  for available_present_mode in available_present_modes {
    if available_present_mode == .MAILBOX {
      return available_present_mode
    }
  }

  return .FIFO
}

choose_swap_extent :: proc(capabilities: vk.SurfaceCapabilitiesKHR) -> vk.Extent2D {
  if capabilities.currentExtent.width != max(u32) && !application.framebuffer_resized {
    return capabilities.currentExtent
  }

  width, height := glfw.GetFramebufferSize(application.window)

  actualExtent := vk.Extent2D {
    width  = cast(u32)width,
    height = cast(u32)height,
  }

  actualExtent.width = clamp(
    actualExtent.width,
    capabilities.minImageExtent.width,
    capabilities.maxImageExtent.width,
  )
  actualExtent.height = clamp(
    actualExtent.height,
    capabilities.minImageExtent.height,
    capabilities.maxImageExtent.height,
  )

  return actualExtent
}

check_device_extension_support :: proc(device: vk.PhysicalDevice) -> bool {
  extension_count: u32
  vk.EnumerateDeviceExtensionProperties(device, nil, &extension_count, nil)

  available_extensions := make([dynamic]vk.ExtensionProperties, extension_count)
  vk.EnumerateDeviceExtensionProperties(
    device,
    nil,
    &extension_count,
    raw_data(available_extensions),
  )

  required_extensions := [dynamic]cstring{}
  append(&required_extensions, ..application.device_extensions[:])

  for extension in &available_extensions {
    ext_name := cstring(&extension.extensionName[0])

    for req_name, index in required_extensions {
      if c.strcmp(ext_name, req_name) == 0 {
        unordered_remove(&required_extensions, index)
      }
    }
  }

  return len(required_extensions) == 0
}

query_swap_chain_support :: proc(device: vk.PhysicalDevice) -> SwapChainSupportDetails {
  details: SwapChainSupportDetails
  vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(
    device,
    application.surface,
    &details.capabilities,
  )

  format_count: u32
  vk.GetPhysicalDeviceSurfaceFormatsKHR(device, application.surface, &format_count, nil)

  if (format_count != 0) {
    resize(&details.formats, cast(int)format_count)
    vk.GetPhysicalDeviceSurfaceFormatsKHR(
      device,
      application.surface,
      &format_count,
      raw_data(details.formats),
    )
  }

  present_mode_count: u32
  vk.GetPhysicalDeviceSurfacePresentModesKHR(
    device,
    application.surface,
    &present_mode_count,
    nil,
  )

  if (present_mode_count != 0) {
    resize(&details.present_modes, cast(int)present_mode_count)
    vk.GetPhysicalDeviceSurfacePresentModesKHR(
      device,
      application.surface,
      &present_mode_count,
      raw_data(details.present_modes),
    )
  }

  return details
}

find_queue_families :: proc(device: vk.PhysicalDevice) -> QueuesIndices {
  queues_indices: QueuesIndices

  queue_family_count: u32 = 0
  vk.GetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nil)

  queue_families := make([dynamic]vk.QueueFamilyProperties, queue_family_count)
  vk.GetPhysicalDeviceQueueFamilyProperties(
    device,
    &queue_family_count,
    raw_data(queue_families),
  )

  for queue_family, i in queue_families {
    if are_queues_complete(queues_indices) {
      break
    }

    uindex := cast(u32)i //casting is ok since there wont be more than 3 digits of queues
    if .GRAPHICS in queue_family.queueFlags {
      queues_indices.graphics_family = uindex
      continue
    }

    present_support: b32 = false
    vk.GetPhysicalDeviceSurfaceSupportKHR(
      device,
      uindex,
      application.surface,
      &present_support,
    )

    if present_support {
      queues_indices.present_family = uindex
      continue
    }
  }

  return queues_indices
}

are_queues_complete :: proc(indices: QueuesIndices) -> bool {
  _, has_graphics := indices.graphics_family.?
  _, has_present := indices.present_family.?
  return has_graphics && has_present
}

check_validation_layer_support :: proc() -> bool {
  layer_count: u32
  vk.EnumerateInstanceLayerProperties(&layer_count, nil)

  available_layers := make([dynamic]vk.LayerProperties, layer_count)
  vk.EnumerateInstanceLayerProperties(&layer_count, raw_data(available_layers[:]))

  // no layers found
  if layer_count == 0 && len(application.validation_layers) != 0 {
    return false
  }

  search_loop: for layer_name in application.validation_layers {
    if layer_name == nil {
      break
    }

    layerFound := false

    for layerProperties, index in &available_layers {
      if index >= cast(int)layer_count {
        break
      }

      layer_available_name := cstring(&layerProperties.layerName[0])
      if c.strcmp(layer_name, layer_available_name) == 0 {
        layerFound = true
        break search_loop
      }
    }

    if (!layerFound) {
      return false
    }
  }

  return true
}

get_required_extensions :: proc() -> []cstring {
  glfw_extensions := glfw.GetRequiredInstanceExtensions()
  glfw_extension_count := len(glfw_extensions)

  extensions: [dynamic]cstring
  for index in 0 ..< glfw_extension_count {
    ext := glfw_extensions[index]
    if ext != nil {
      append(&extensions, ext)
    }
  }

  if application.enable_validation_layers {
    append(&extensions, vk.EXT_DEBUG_UTILS_EXTENSION_NAME)
  }

  return extensions[:]
}

glfw_set_proc_address :: proc(ptr: rawptr, c_name: cstring) {
  found: bool
  (^rawptr)(ptr)^ = glfw.GetInstanceProcAddress(nil, c_name)
  if ptr == nil {
    name := string(c_name)
    log.warnf("Vulkan proc '%s' not found", name)
  }
}

debug_callback :: proc "c" (
  message_severity: vk.DebugUtilsMessageSeverityFlagsEXT,
  message_types: vk.DebugUtilsMessageTypeFlagsEXT,
  callback_data: ^vk.DebugUtilsMessengerCallbackDataEXT,
  pUserData: rawptr,
) -> b32 {
  context = get_logger()

  if .ERROR in message_severity do log.error(callback_data.pMessage)
  if .WARNING in message_severity do log.warn(callback_data.pMessage)
  if .INFO in message_severity ||.VERBOSE in message_severity do log.info(callback_data.pMessage)

  return false
}

read_file :: proc(file_path: string) -> ([]byte, bool) {
  return os.read_entire_file(file_path)
}
