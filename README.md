# Vulkan Triangle
This is a demo of drawing a triangle on screen using vulkan in odin-lang.

## Dependencies
- You need to have vulkan sdk installed on your system.  
- To run `compile_shaders.sh` you need to have `glslc` installed on your system and on declared in your `path` variable  
- And of course odin-lang compiler version `dev-2022-02-nightly:a70dde34` or above

## Run
To run compile it for your arch/os using:  
```
./compile_and_run.sh
```
This will autoamtically run the demo.

